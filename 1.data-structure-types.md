Sure! Here are some commonly used data structures:

1. Array: A contiguous block of memory used to store elements of the same type.

2. Linked List: A collection of nodes where each node contains data and a reference (or link) to the next node in the sequence.

3. Stack: A last-in, first-out (LIFO) data structure where elements are inserted and removed from the same end (top).

4. Queue: A first-in, first-out (FIFO) data structure where elements are inserted at one end (rear) and removed from the other end (front).

5. Hash Table: A data structure that uses hash functions to map keys to values, allowing for efficient insertion, deletion, and retrieval operations.

6. Tree: A hierarchical data structure composed of nodes, where each node can have zero or more child nodes.

7. Binary Tree: A tree data structure where each node has at most two child nodes, referred to as the left child and right child.

8. Binary Search Tree (BST): A binary tree where the left child of a node contains a value less than the node, and the right child contains a value greater than the node.

9. Heap: A specialized tree-based data structure that satisfies the heap property. It can be a min heap (the value of each node is greater than or equal to its parent) or a max heap (the value of each node is less than or equal to its parent).

10. Graph: A collection of nodes (vertices) and edges that connect pairs of nodes.

11. Hash Map: An implementation of a map or dictionary using a hash table to store key-value pairs.

12. Set: A collection of unique elements with no defined order.

13. Trie: A tree-like data structure used for efficient retrieval of keys in a large set of strings.

14. Stack (abstract data type): A data structure that follows the LIFO principle, where elements are inserted and removed from the top.

15. Queue (abstract data type): A data structure that follows the FIFO principle, where elements are inserted at the rear and removed from the front.

These are just a few examples, and there are many other data structures and variations available depending on specific use cases and requirements.